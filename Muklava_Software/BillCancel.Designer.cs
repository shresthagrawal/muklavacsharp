﻿namespace Muklava_Software
{
    partial class BillCancel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.transictionIdText = new System.Windows.Forms.TextBox();
            this.cancelBillBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Transiction Id:";
            // 
            // transictionIdText
            // 
            this.transictionIdText.Location = new System.Drawing.Point(93, 10);
            this.transictionIdText.Name = "transictionIdText";
            this.transictionIdText.Size = new System.Drawing.Size(100, 20);
            this.transictionIdText.TabIndex = 1;
            // 
            // cancelBillBtn
            // 
            this.cancelBillBtn.Location = new System.Drawing.Point(93, 52);
            this.cancelBillBtn.Name = "cancelBillBtn";
            this.cancelBillBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBillBtn.TabIndex = 2;
            this.cancelBillBtn.Text = "Cancel Bill";
            this.cancelBillBtn.UseVisualStyleBackColor = true;
            this.cancelBillBtn.Click += new System.EventHandler(this.cancelBillBtn_Click);
            // 
            // BillCancel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(246, 140);
            this.Controls.Add(this.cancelBillBtn);
            this.Controls.Add(this.transictionIdText);
            this.Controls.Add(this.label1);
            this.Name = "BillCancel";
            this.Text = "BillCancel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox transictionIdText;
        private System.Windows.Forms.Button cancelBillBtn;
    }
}