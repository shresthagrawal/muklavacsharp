﻿namespace Muklava_Software
{
    partial class CustomerData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gdv = new System.Windows.Forms.DataGridView();
            this.print = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gdv)).BeginInit();
            this.SuspendLayout();
            // 
            // gdv
            // 
            this.gdv.AllowUserToAddRows = false;
            this.gdv.AllowUserToDeleteRows = false;
            this.gdv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdv.Location = new System.Drawing.Point(12, 12);
            this.gdv.Name = "gdv";
            this.gdv.ReadOnly = true;
            this.gdv.Size = new System.Drawing.Size(776, 393);
            this.gdv.TabIndex = 0;
            // 
            // print
            // 
            this.print.Location = new System.Drawing.Point(713, 415);
            this.print.Name = "print";
            this.print.Size = new System.Drawing.Size(75, 23);
            this.print.TabIndex = 1;
            this.print.Text = "Print";
            this.print.UseVisualStyleBackColor = true;
            this.print.Click += new System.EventHandler(this.print_Click);
            // 
            // CustomerData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.print);
            this.Controls.Add(this.gdv);
            this.Name = "CustomerData";
            this.Text = "CustomerData";
            this.Load += new System.EventHandler(this.CustomerData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gdv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gdv;
        private System.Windows.Forms.Button print;
    }
}