﻿namespace Muklava_Software
{
    partial class measurementRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tb1 = new System.Windows.Forms.TextBox();
            this.tb2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb12 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb13 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb14 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tb4 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tb5 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tb11 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tb10 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tb9 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tb7 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tb6 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tb22 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tb21 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tb20 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tb19 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tb18 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tb17 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tb16 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tb15 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tb26 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tb24 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tb23 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tb33 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.tb32 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tb31 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.tb30 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.tb29 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.tb28 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.tb27 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tb25 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Length";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // tb1
            // 
            this.tb1.Location = new System.Drawing.Point(74, 50);
            this.tb1.Name = "tb1";
            this.tb1.Size = new System.Drawing.Size(100, 20);
            this.tb1.TabIndex = 1;
            // 
            // tb2
            // 
            this.tb2.Location = new System.Drawing.Point(74, 89);
            this.tb2.Name = "tb2";
            this.tb2.Size = new System.Drawing.Size(100, 20);
            this.tb2.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Shoulder";
            // 
            // tb12
            // 
            this.tb12.Location = new System.Drawing.Point(74, 440);
            this.tb12.Name = "tb12";
            this.tb12.Size = new System.Drawing.Size(100, 20);
            this.tb12.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 443);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Front Neeck";
            // 
            // tb13
            // 
            this.tb13.Location = new System.Drawing.Point(74, 479);
            this.tb13.Name = "tb13";
            this.tb13.Size = new System.Drawing.Size(100, 20);
            this.tb13.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 482);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Back Neck";
            // 
            // tb3
            // 
            this.tb3.Location = new System.Drawing.Point(74, 129);
            this.tb3.Name = "tb3";
            this.tb3.Size = new System.Drawing.Size(100, 20);
            this.tb3.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Upp Chest";
            // 
            // tb14
            // 
            this.tb14.Location = new System.Drawing.Point(74, 516);
            this.tb14.Name = "tb14";
            this.tb14.Size = new System.Drawing.Size(100, 20);
            this.tb14.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 519);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Dp";
            // 
            // tb4
            // 
            this.tb4.Location = new System.Drawing.Point(74, 164);
            this.tb4.Name = "tb4";
            this.tb4.Size = new System.Drawing.Size(100, 20);
            this.tb4.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 167);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Chest";
            // 
            // tb5
            // 
            this.tb5.Location = new System.Drawing.Point(74, 202);
            this.tb5.Name = "tb5";
            this.tb5.Size = new System.Drawing.Size(100, 20);
            this.tb5.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 205);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Waist";
            // 
            // tb11
            // 
            this.tb11.Location = new System.Drawing.Point(74, 399);
            this.tb11.Name = "tb11";
            this.tb11.Size = new System.Drawing.Size(100, 20);
            this.tb11.TabIndex = 29;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 402);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "3/4 Sleve";
            // 
            // tb10
            // 
            this.tb10.Location = new System.Drawing.Point(74, 361);
            this.tb10.Name = "tb10";
            this.tb10.Size = new System.Drawing.Size(100, 20);
            this.tb10.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(14, 364);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Elbow";
            // 
            // tb9
            // 
            this.tb9.Location = new System.Drawing.Point(74, 326);
            this.tb9.Name = "tb9";
            this.tb9.Size = new System.Drawing.Size(100, 20);
            this.tb9.TabIndex = 25;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(14, 329);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Bicep";
            // 
            // tb7
            // 
            this.tb7.Location = new System.Drawing.Point(74, 286);
            this.tb7.Name = "tb7";
            this.tb7.Size = new System.Drawing.Size(100, 20);
            this.tb7.TabIndex = 23;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(14, 289);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "Arm Hole";
            // 
            // tb6
            // 
            this.tb6.Location = new System.Drawing.Point(74, 247);
            this.tb6.Name = "tb6";
            this.tb6.Size = new System.Drawing.Size(100, 20);
            this.tb6.TabIndex = 21;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(14, 250);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "Low Waist";
            // 
            // tb22
            // 
            this.tb22.Location = new System.Drawing.Point(269, 326);
            this.tb22.Name = "tb22";
            this.tb22.Size = new System.Drawing.Size(100, 20);
            this.tb22.TabIndex = 45;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(209, 329);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 44;
            this.label8.Text = "Neck Style";
            // 
            // tb21
            // 
            this.tb21.Location = new System.Drawing.Point(269, 286);
            this.tb21.Name = "tb21";
            this.tb21.Size = new System.Drawing.Size(100, 20);
            this.tb21.TabIndex = 43;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(209, 289);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 13);
            this.label10.TabIndex = 42;
            this.label10.Text = "Sleeve Len";
            // 
            // tb20
            // 
            this.tb20.Location = new System.Drawing.Point(269, 247);
            this.tb20.Name = "tb20";
            this.tb20.Size = new System.Drawing.Size(100, 20);
            this.tb20.TabIndex = 41;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(209, 250);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 13);
            this.label16.TabIndex = 40;
            this.label16.Text = "Chak Open";
            // 
            // tb19
            // 
            this.tb19.Location = new System.Drawing.Point(269, 202);
            this.tb19.Name = "tb19";
            this.tb19.Size = new System.Drawing.Size(100, 20);
            this.tb19.TabIndex = 39;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(209, 205);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(34, 13);
            this.label17.TabIndex = 38;
            this.label17.Text = "Panel";
            // 
            // tb18
            // 
            this.tb18.Location = new System.Drawing.Point(269, 164);
            this.tb18.Name = "tb18";
            this.tb18.Size = new System.Drawing.Size(100, 20);
            this.tb18.TabIndex = 37;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(209, 167);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(28, 13);
            this.label18.TabIndex = 36;
            this.label18.Text = "Hips";
            // 
            // tb17
            // 
            this.tb17.Location = new System.Drawing.Point(269, 129);
            this.tb17.Name = "tb17";
            this.tb17.Size = new System.Drawing.Size(100, 20);
            this.tb17.TabIndex = 35;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(209, 132);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(34, 13);
            this.label19.TabIndex = 34;
            this.label19.Text = "Waist";
            // 
            // tb16
            // 
            this.tb16.Location = new System.Drawing.Point(269, 89);
            this.tb16.Name = "tb16";
            this.tb16.Size = new System.Drawing.Size(100, 20);
            this.tb16.TabIndex = 33;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(209, 92);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(34, 13);
            this.label20.TabIndex = 32;
            this.label20.Text = "Chest";
            // 
            // tb15
            // 
            this.tb15.Location = new System.Drawing.Point(269, 50);
            this.tb15.Name = "tb15";
            this.tb15.Size = new System.Drawing.Size(100, 20);
            this.tb15.TabIndex = 31;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(209, 53);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(40, 13);
            this.label21.TabIndex = 30;
            this.label21.Text = "Length";
            // 
            // tb26
            // 
            this.tb26.Location = new System.Drawing.Point(496, 326);
            this.tb26.Name = "tb26";
            this.tb26.Size = new System.Drawing.Size(68, 20);
            this.tb26.TabIndex = 61;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(404, 329);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(86, 13);
            this.label22.TabIndex = 60;
            this.label22.Text = "Hanging Des No";
            // 
            // tb24
            // 
            this.tb24.Location = new System.Drawing.Point(464, 89);
            this.tb24.Name = "tb24";
            this.tb24.Size = new System.Drawing.Size(100, 20);
            this.tb24.TabIndex = 49;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(404, 92);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(34, 13);
            this.label28.TabIndex = 48;
            this.label28.Text = "Waist";
            // 
            // tb23
            // 
            this.tb23.Location = new System.Drawing.Point(464, 50);
            this.tb23.Name = "tb23";
            this.tb23.Size = new System.Drawing.Size(100, 20);
            this.tb23.TabIndex = 47;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(404, 53);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(40, 13);
            this.label29.TabIndex = 46;
            this.label29.Text = "Length";
            // 
            // tb33
            // 
            this.tb33.Location = new System.Drawing.Point(687, 326);
            this.tb33.Name = "tb33";
            this.tb33.Size = new System.Drawing.Size(75, 20);
            this.tb33.TabIndex = 77;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(602, 329);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(79, 13);
            this.label30.TabIndex = 76;
            this.label30.Text = "Color Shde No.";
            // 
            // tb32
            // 
            this.tb32.Location = new System.Drawing.Point(662, 247);
            this.tb32.Name = "tb32";
            this.tb32.Size = new System.Drawing.Size(100, 20);
            this.tb32.TabIndex = 73;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(602, 250);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(33, 13);
            this.label32.TabIndex = 72;
            this.label32.Text = "Mohri";
            // 
            // tb31
            // 
            this.tb31.Location = new System.Drawing.Point(662, 202);
            this.tb31.Name = "tb31";
            this.tb31.Size = new System.Drawing.Size(100, 20);
            this.tb31.TabIndex = 71;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(602, 205);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(37, 13);
            this.label33.TabIndex = 70;
            this.label33.Text = "Knees";
            // 
            // tb30
            // 
            this.tb30.Location = new System.Drawing.Point(662, 164);
            this.tb30.Name = "tb30";
            this.tb30.Size = new System.Drawing.Size(100, 20);
            this.tb30.TabIndex = 69;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(602, 167);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(34, 13);
            this.label34.TabIndex = 68;
            this.label34.Text = "Thigh";
            // 
            // tb29
            // 
            this.tb29.Location = new System.Drawing.Point(662, 129);
            this.tb29.Name = "tb29";
            this.tb29.Size = new System.Drawing.Size(100, 20);
            this.tb29.TabIndex = 67;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(602, 132);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(28, 13);
            this.label35.TabIndex = 66;
            this.label35.Text = "Hips";
            // 
            // tb28
            // 
            this.tb28.Location = new System.Drawing.Point(662, 89);
            this.tb28.Name = "tb28";
            this.tb28.Size = new System.Drawing.Size(100, 20);
            this.tb28.TabIndex = 65;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(602, 92);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(34, 13);
            this.label36.TabIndex = 64;
            this.label36.Text = "Waist";
            // 
            // tb27
            // 
            this.tb27.Location = new System.Drawing.Point(662, 50);
            this.tb27.Name = "tb27";
            this.tb27.Size = new System.Drawing.Size(100, 20);
            this.tb27.TabIndex = 63;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(602, 53);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(40, 13);
            this.label37.TabIndex = 62;
            this.label37.Text = "Length";
            // 
            // tb25
            // 
            this.tb25.Location = new System.Drawing.Point(464, 129);
            this.tb25.Name = "tb25";
            this.tb25.Size = new System.Drawing.Size(100, 20);
            this.tb25.TabIndex = 79;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(404, 132);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(0, 13);
            this.label23.TabIndex = 78;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(410, 132);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(28, 13);
            this.label24.TabIndex = 80;
            this.label24.Text = "Hips";
            // 
            // measurementRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 565);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.tb25);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.tb33);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.tb32);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.tb31);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.tb30);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.tb29);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.tb28);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.tb27);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.tb26);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.tb24);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.tb23);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.tb22);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tb21);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tb20);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.tb19);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.tb18);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.tb17);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.tb16);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.tb15);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.tb11);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tb10);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tb9);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tb7);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.tb6);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.tb5);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tb4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tb14);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tb3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tb13);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb12);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb1);
            this.Controls.Add(this.label1);
            this.Name = "measurementRecord";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.measurementRecord_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb1;
        private System.Windows.Forms.TextBox tb2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb12;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb13;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb14;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb11;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tb10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tb7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tb6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tb22;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb21;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb20;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tb19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tb18;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tb17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tb16;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tb15;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tb26;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tb24;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tb23;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tb33;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox tb32;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox tb31;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox tb30;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox tb29;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox tb28;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox tb27;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tb25;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
    }
}