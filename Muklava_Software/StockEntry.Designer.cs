﻿namespace Muklava_Software
{
    partial class StockEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bntCapture = new System.Windows.Forms.Button();
            this.imgCapture = new System.Windows.Forms.PictureBox();
            this.imgVideo = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.partyname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.mrp = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checklistbranch = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.done = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.id = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.BNo = new System.Windows.Forms.TextBox();
            this.sizetext = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.imgCapture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVideo)).BeginInit();
            this.SuspendLayout();
            // 
            // bntCapture
            // 
            this.bntCapture.Location = new System.Drawing.Point(86, 289);
            this.bntCapture.Name = "bntCapture";
            this.bntCapture.Size = new System.Drawing.Size(85, 23);
            this.bntCapture.TabIndex = 14;
            this.bntCapture.Text = "&Capture Image";
            this.bntCapture.UseVisualStyleBackColor = true;
            this.bntCapture.Click += new System.EventHandler(this.bntCapture_Click);
            // 
            // imgCapture
            // 
            this.imgCapture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgCapture.Location = new System.Drawing.Point(500, 272);
            this.imgCapture.Name = "imgCapture";
            this.imgCapture.Size = new System.Drawing.Size(532, 238);
            this.imgCapture.TabIndex = 10;
            this.imgCapture.TabStop = false;
            this.imgCapture.Click += new System.EventHandler(this.imgCapture_Click);
            // 
            // imgVideo
            // 
            this.imgVideo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgVideo.Location = new System.Drawing.Point(500, 26);
            this.imgVideo.Name = "imgVideo";
            this.imgVideo.Size = new System.Drawing.Size(532, 225);
            this.imgVideo.TabIndex = 9;
            this.imgVideo.TabStop = false;
            this.imgVideo.Click += new System.EventHandler(this.imgVideo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Item_Name";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(133, 36);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(320, 20);
            this.name.TabIndex = 17;
            this.name.TextChanged += new System.EventHandler(this.name_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "PartyName";
            // 
            // partyname
            // 
            this.partyname.Location = new System.Drawing.Point(133, 79);
            this.partyname.Name = "partyname";
            this.partyname.Size = new System.Drawing.Size(320, 20);
            this.partyname.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(62, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "M.R.P";
            // 
            // mrp
            // 
            this.mrp.Location = new System.Drawing.Point(133, 118);
            this.mrp.Name = "mrp";
            this.mrp.Size = new System.Drawing.Size(320, 20);
            this.mrp.TabIndex = 19;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(177, 289);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "&Next";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checklistbranch
            // 
            this.checklistbranch.FormattingEnabled = true;
            this.checklistbranch.Location = new System.Drawing.Point(133, 155);
            this.checklistbranch.Name = "checklistbranch";
            this.checklistbranch.Size = new System.Drawing.Size(320, 49);
            this.checklistbranch.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(62, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Branch";
            // 
            // done
            // 
            this.done.Location = new System.Drawing.Point(268, 289);
            this.done.Name = "done";
            this.done.Size = new System.Drawing.Size(119, 23);
            this.done.TabIndex = 23;
            this.done.Text = "&Print and Complete";
            this.done.UseVisualStyleBackColor = true;
            this.done.Click += new System.EventHandler(this.done_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(60, 337);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Product Id:";
            // 
            // id
            // 
            this.id.AutoSize = true;
            this.id.Location = new System.Drawing.Point(125, 337);
            this.id.Name = "id";
            this.id.Size = new System.Drawing.Size(0, 13);
            this.id.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(61, 387);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "PrintBarcodeNumber";
            // 
            // BNo
            // 
            this.BNo.Location = new System.Drawing.Point(172, 384);
            this.BNo.Name = "BNo";
            this.BNo.Size = new System.Drawing.Size(37, 20);
            this.BNo.TabIndex = 27;
            this.BNo.Text = "0";
            this.BNo.TextChanged += new System.EventHandler(this.BNo_TextChanged);
            // 
            // sizetext
            // 
            this.sizetext.Location = new System.Drawing.Point(132, 213);
            this.sizetext.Name = "sizetext";
            this.sizetext.Size = new System.Drawing.Size(320, 20);
            this.sizetext.TabIndex = 29;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(61, 216);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "Size";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 487);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 30;
            this.button2.Text = "Configure";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // StockEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 522);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.sizetext);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.BNo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.id);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.done);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checklistbranch);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.partyname);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.mrp);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.name);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bntCapture);
            this.Controls.Add(this.imgCapture);
            this.Controls.Add(this.imgVideo);
            this.Name = "StockEntry";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgCapture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVideo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button bntCapture;
        private System.Windows.Forms.PictureBox imgCapture;
        private System.Windows.Forms.PictureBox imgVideo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox partyname;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox mrp;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckedListBox checklistbranch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button done;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label id;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox BNo;
        private System.Windows.Forms.TextBox sizetext;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button2;
    }
}