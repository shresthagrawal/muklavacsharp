﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Muklava_Software
{
   
    class Helper
    {

        public static void SaveImageCapture(System.Drawing.Image image, string name)
        {

            SaveFileDialog s = new SaveFileDialog();
            s.FileName = name;// Default file name
            s.DefaultExt = ".Jpg";// Default file extension
            s.Filter = "Image (.jpg)|*.jpg"; // Filter files by extension


           // if (s.ShowDialog() == DialogResult.OK)
            {
                // Save Image
                
                string filename =  Salabs.getpath(s.FileName);
                //filename = Salabs.getdocumentspath();
                FileStream fstream = new FileStream(filename, FileMode.Create, FileAccess.ReadWrite);
                image.Save(fstream, System.Drawing.Imaging.ImageFormat.Jpeg);
                fstream.Close();

            }

        }
    }
}
