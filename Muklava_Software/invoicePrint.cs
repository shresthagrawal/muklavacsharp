﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Muklava_Software
{
    public partial class invoicePrint : Form
    {
        string transictionId="133";
        public invoicePrint(string tid)
        {
            transictionId = tid;
            InitializeComponent();
        }
       
        private void invoicePrint_Load(object sender, EventArgs e)
        {
            
       

            //dataReader = salabs.DBMSQuerry("SELECT productid,productname,partyname,mrp,size FROM product WHERE (productid>=" + txtmin.Text + ")AND(productid<=" + txtmax.Text + ")");

            MySqlDataReader dataReader;
            Salabs salabs = new Salabs();
            
            invoiceData invoiceData = new invoiceData();
            DataTable productSold = invoiceData.productSold;
            DataTable otherdetails = invoiceData.otherDetails;
            Invoice Report = new Invoice();
            DataRow details = otherdetails.NewRow();
            


            dataReader = salabs.DBMSQuerry("SELECT * FROM NullStock WHERE TransictionId='" + transictionId + "'");

            int i = 1;
            int total=0;
            String Branch="";
            while(dataReader.Read())
            {

                DataRow drow = productSold.NewRow();

                drow["S.No."] = i.ToString();
                drow["Paticulars"] = dataReader["ProductId"]+" "+dataReader["Name"];
                drow["Amount"] = dataReader["Mrp"];
                Branch= dataReader["Branch"].ToString();
                total +=Convert.ToInt32(dataReader["Mrp"]);
                productSold.Rows.Add(drow);
                i++;
            }
            dataReader = salabs.DBMSQuerry("SELECT * FROM CustomerTransiction WHERE TransictionId='" + transictionId + "'");

           
            






            if (Branch.Equals("Pandri"))
            {
                details["contactdetails"] = "# 73, Mahalaxmi Cloth Market,Pandri,Raipur(C.G.) Ph:0771-4001004|9713134800";
            }
            else
            {
                details["contactdetails"] = "# 117, Block A 1st Floor,Crystal Arcade,Shankarnagar ,Raipur(C.G.) Ph:0771-4004880|9713134800";

            }
            while (dataReader.Read())
            {
                if (Convert.ToInt32(dataReader["addCharges"])>0)
                { DataRow addCharges = productSold.NewRow();
                    addCharges["S.No."] = i.ToString();
                    addCharges["Paticulars"] = dataReader["addChargesLabel"];
                    addCharges["Amount"] = dataReader["addCharges"];
                    total += Convert.ToInt32(dataReader["addCharges"]);
                    productSold.Rows.Add(addCharges);
                }
                details["to"] = dataReader["Name"]; ;
                details["date"] = dataReader["CurrentdateTime"];
                details["phonenumber"] = dataReader["Number"]; 
                details["total"] = dataReader["TotalPrice"]; 
                details["discount"] = Convert.ToInt32(dataReader["Discount"].ToString()) > 0 ? dataReader["Discount"]:"";
                details["boolDiscount"] = Convert.ToInt32(dataReader["Discount"].ToString()) > 0 ? "Discount: " : "";
                details["totalAmountPaid"] =(Convert.ToInt32(dataReader["Totalprice"])-Convert.ToInt32(dataReader["credit"])).ToString();
                details["DueAmount"] = dataReader["credit"];
            }

            details["socialmedia"] = "www.muklava.com | fb.com/muklava.raipur";
           
            details["invoiceno"] = transictionId.ToString();
           
            details["details1"] = "Goods once sold will not be exchanged or return back. No guarentee for colour and zari";
            details["details2"] = "*MuklavaIsTheBest.1234*";
            otherdetails.Rows.Add(details);
            Report.Database.Tables["otherDetails"].SetDataSource((DataTable)otherdetails);
            Report.Database.Tables["productSold"].SetDataSource((DataTable)productSold);
            invoiceView main1 = new invoiceView(Report);
            main1.Show();
            this.Close();
        }
    }
}
