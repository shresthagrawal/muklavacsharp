﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Muklava_Software
{
    public partial class BillingNew : Form
    {
        public BillingNew()
        {
            InitializeComponent();
        }
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        Salabs dbms = new Salabs();
        Boolean Check = false;
        Boolean checkClicked = false;
        int Discount;
        MySqlDataReader dataReader;
        MySqlDataReader currentProductData;
        int totalval = 0;
        int totaldiscount = 0;
        int idDevice = 0;
        String masterQuery = "";

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public void BindData()
        {


        }

        private void button1_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        public static BackgroundWorker billingTask = new BackgroundWorker();
        private void backgroundTaskAfterBilling(object sender, DoWorkEventArgs e)
        {
            String Id1 = txtpoints1.Text;
            String Id2 = txtpoints2.Text;
            String points = TotalLbl.Text;
            String querry = "UPDATE CustomerRecord SET points = points +" + (totalval / 100).ToString() + " where PhoneNumber ='" + phonenumber.Text + "'";
            dbms.DBMSQuerry(querry);
           
            String querryPoints = "UPDATE Points " +
                               "SET lastMonthsPoints = Points.Points, Points = 0 " +
                               "WHERE Not Month(TimeStamp) = Month(CURDATE()); " +
                               "INSERT INTO Points SET Staffid=" + Id1 + ", Points =0 " +
                               "ON DUPLICATE KEY UPDATE staffid = staffid; " +
                               "INSERT INTO Points SET Staffid=" + Id2 + ", Points =0 " +
                               "ON DUPLICATE KEY UPDATE staffid = staffid; " +
                               "Update Points SET Points = Points+ " + points + " WHERE staffid = " + Id1 + ";" +
                               "Update Points SET Points = Points + ((" + points + ") / 2)  WHERE staffid = " + Id2 + ";";

            if (Salabs.CheckForInternetConnection()) {
                dbms.DBMSQuerry(querryPoints, sender, MuklavaFuntion.connStringMuklavaOnline, 0, 0);
                MuklavaFuntion.createPushNotification("NewBill", tid.Text + " Customer Name:" + CustomerName.Text + " | " + "Bill Amount:" + totalval.ToString(), ";");
            }

        }



        private void Print_Click(object sender, EventArgs e)
        {
            if (Check == true)
            {
           
            masterQuery= masterQuery+"INSERT INTO CustomerTransiction SET  Number = '" + phonenumber.Text + "',Name = '" + CustomerName.Text + "', TransictionId = '" + tid.Text + "', TotalPrice = '" + TotalLbl.Text + "', credit = '" + TotalLbl.Text + "',discount = '" + totaldiscount.ToString() + "',addCharges = '" + addCharges.ToString() + "',addChargesLabel = '" + addChargesLabelText.Text.ToString() + "'; ";
            masterQuery = masterQuery + "SELECT Max(TransictionId) FROM CustomerTransiction where Number = '" + phonenumber.Text + "' AND Name = '" + CustomerName.Text + "' AND  TotalPrice = '" + TotalLbl.Text + "' AND credit = '" + TotalLbl.Text + "'AND discount = '" + totaldiscount.ToString() + "';";
            dataReader = dbms.DBMSQuerry(masterQuery);

            int idOnServer=0;
            while (dataReader.Read())
            {
                idOnServer = (Convert.ToInt32((dataReader["Max(TransictionId)"])));
            }
            if(idOnServer!=idDevice)
                {
                    MessageBox.Show("ERROR, PLS CANCEL THE BILL AND REBILL");
                    return;
                }
            
            dueamount main1 = new dueamount(phonenumber.Text, idOnServer);
            main1.Show();

            billingTask.DoWork += new DoWorkEventHandler(backgroundTaskAfterBilling);
            billingTask.RunWorkerAsync();

            this.Close();
                
               
            }
            else
            {
                MessageBox.Show("Please enter a customer number and press strart to add the product !!");
            }
        }

        public void changetext(string str)
        {
            productid.Text = str;
        }

        private void billing_Load(object sender, EventArgs e)
        {
            
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("productName", typeof(string));
            dt.Columns.Add("Mrp", typeof(decimal));

           /* NullData.Columns.Add("ID", typeof(int));
            NullData.Columns.Add("productName", typeof(string));
            NullData.Columns.Add("Mrp", typeof(decimal));*/

            dt1.Columns.Add("ID", typeof(int));
            dt1.Columns.Add("productName", typeof(string));
            dt1.Columns.Add("Mrp", typeof(decimal));

           

            GridView1.DataSource = dt;
            addview.DataSource = dt1;
            //  GridView1.DataBind();

            dataReader = dbms.DBMSQuerry("SELECT Max(TransictionId) FROM CustomerTransiction");
            
            while (dataReader.Read())
            {
                tid.Text = ((Convert.ToInt32(dataReader["Max(TransictionId)"])+1).ToString());
                idDevice = (Convert.ToInt32(dataReader["Max(TransictionId)"]) + 1);
            }
        }

        public void start_Click(object sender, EventArgs e)
        {

            //dbms.Querry("INSERT INTO product SET  productname = 'appple',partyname = 'appple',mrp = 200");

            dataReader = dbms.DBMSQuerry("SELECT Name,Points FROM CustomerRecord WHERE phonenumber ='" + phonenumber.Text + "'",sender);
            while (dataReader.Read())
            {
                
                if (!dataReader["Name"].Equals(null))
                {

                    Check = true;
                    CustomerName.Text = (dataReader["Name"] + "");
                    pointslbl.Text = (dataReader["Points"] + "");
                    discountvalue.Text = (Convert.ToInt32(dataReader["Points"]) * 10).ToString();
                }
                
            }
           
            if (Check != true)
            {
               
                CreateCustomer cr = new CreateCustomer(phonenumber.Text, true);
                cr.Show();
                //start_Click(sender, e);

            }


        }
        
        private void add_Click(object sender, EventArgs e)
        {
                if(checkClicked)
                {
                    checkClicked = false;
                    dt.Rows.Add(currentProductData["productid"], currentProductData["productname"], currentProductData["mrp"]);
                    GridView1.DataSource = dt;
                   
                    String query1 = "INSERT INTO NullStock SET  ProductId = '" + currentProductData["productid"]  + "',Name = '" + currentProductData["productname"] + "', PartyName = '" + currentProductData["partyname"] + "', TransictionId = '" + tid.Text + "', Branch = '" + currentProductData["branch"] + "', Mrp = '" + currentProductData["mrp"] + "'; ";
                    String query2 = "DELETE FROM product WHERE productid ='" + currentProductData["productid"] + "'; ";
                    masterQuery= masterQuery+ query1 + query2;
                    totalval += Convert.ToInt32(currentProductData["mrp"]);
                    TotalLbl.Text = totalval.ToString();
                    dt1.Rows.Remove(dt1.Rows[0]);
                    addview.DataSource = dt1;
                }
           


        }
       

        private void productid_TextChanged(object sender, EventArgs e)
        {


        }

        private void CustomerName_Click(object sender, EventArgs e)
        {

        }
        
        private void CheckBtn_Click(object sender, EventArgs e)
        {

            if (dt1.Rows.Count>0)
            {
                dt1.Rows.Remove(dt1.Rows[0]);
            }
            currentProductData = dbms.DBMSQuerry("SELECT * FROM product WHERE productid ='" + productid.Text + "'");
            MySqlDataReader checkRead = currentProductData;
            while (checkRead.Read())
            {
                checkClicked = true;
                dt1.Rows.Add(productid.Text, checkRead["productname"], checkRead["mrp"]);
            }
            addview.DataSource = dt1;

        }
        Salabs salabs = new Salabs();

        private void reedem_Click(object sender, EventArgs e)
        {
            String querry = "UPDATE CustomerRecord SET points = 0 where PhoneNumber ='"+phonenumber.Text+"'";
            salabs.DBMSQuerry(querry);
            Discount = Convert.ToInt32(discountvalue.Text);
            TotalLbl.Text = (Convert.ToInt32(TotalLbl.Text) - Discount).ToString();
            MessageBox.Show("Reedem Successfull");
            totalval = totalval - Discount;
            totaldiscount =totaldiscount+ Discount;
        }

        private void discountvalue_TextChanged(object sender, EventArgs e)
        {

        }

        private void discountvalue_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Discount = Convert.ToInt32(discountvalue2.Text);
            TotalLbl.Text = (Convert.ToInt32(TotalLbl.Text) - Discount).ToString();
            MessageBox.Show("Reedem Successfull");
            totalval = totalval - Discount;
            totaldiscount = totaldiscount + Discount;
        }
        int addCharges=0;
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            totalval -= addCharges;
            try
            {
                addCharges = Convert.ToInt32(txtAddCharges.Text);
            }
            catch
            {
                addCharges += 0;
            }
            
            totalval += addCharges;
            TotalLbl.Text = totalval.ToString();
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CreateProduct createProduct = new CreateProduct(this);
            createProduct.Show();
        }

        private void txtpoints1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
