﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Muklava_Software
{
    public partial class photosUpload : Form
    {
        int Start, End, Unsuccesful=0;
        public photosUpload(int start=0, int end=0)
        {
            
            InitializeComponent();
            txtStart.Text = start.ToString();
            txtEnd.Text = end.ToString();
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void photosUpload_Load(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            Start =Convert.ToInt32(txtStart.Text);
            End = Convert.ToInt32(txtEnd.Text);
            progressBar.Minimum = 0;
            progressBar.Maximum = End-Start+1;
            button1.Visible = false;
            upload();
           
        }
       

        public static BackgroundWorker myWorker = new BackgroundWorker();

        public void upload()
        {
            myWorker.DoWork += new DoWorkEventHandler(myWorker_DoWork);
            myWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(myWorker_RunWorkerCompleted);
            myWorker.ProgressChanged += new ProgressChangedEventHandler(myWorker_ProgressChanged);
            myWorker.WorkerReportsProgress = true;
            myWorker.WorkerSupportsCancellation = true;
            myWorker.RunWorkerAsync();
        }

        private void myWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value += 1;
        }

        private void myWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Succesfully Completed| Unsuccesfull Atempts: " + Unsuccesful+"|"+e.Result);
            button1.Visible = true;

        }
        Salabs salabs = new Salabs();
        private void myWorker_DoWork(object sender, DoWorkEventArgs e)
        {
           // MessageBox.Show("error");
            BackgroundWorker sendingWorker =
                (BackgroundWorker)sender;

            for (int i = Start; i <= End; i++)
            {

                if (!sendingWorker.CancellationPending)//At each iteration of the loop, 
                                                       //check if there is a cancellation request pending 
                {
                    String id = i.ToString();
                    try
                    {
                        salabs.FileUpload("ftp://muklava.com/muklava/" + id + ".jpeg", Salabs.getpath(id));
                    }
                    catch
                    {
                        MessageBox.Show("error");
                        Unsuccesful++;
                    }
                    sendingWorker.ReportProgress(i);//Report our progress to the main thread
                }
                else
                {
                    e.Cancel = true;//If a cancellation request is pending, assign this flag a value of true
                    break;// If a cancellation request is pending, break to exit the loop
                }
            }
            e.Result = "done";
        }
    }
}
