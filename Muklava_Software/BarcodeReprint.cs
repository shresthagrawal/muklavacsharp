﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Muklava_Software
{
    public partial class BarcodeReprint : Form
    {
        public BarcodeReprint()
        {
            InitializeComponent();
        }
        MySqlDataReader dataReader;
        DataTable dt = new DataTable();
        Salabs salabs = new Salabs();
        int EmptyBarcode=0,minval=0;
        public BarcodeReprint(int min, int blankcode)
        {
            minval= min;
            EmptyBarcode = blankcode;
            InitializeComponent();

        }

      

        private void button1_Click(object sender, EventArgs e)
        {
           
            



            
                dataReader = salabs.DBMSQuerry("SELECT productid,productname,partyname,mrp,size FROM product WHERE (productid>="+txtmin.Text+")AND(productid<="+txtmax.Text+")");

           
            BarcodeData barcodeDetails = new BarcodeData();
            DataTable dataTable = barcodeDetails.BarcodeTable;
            BarcodeRpt Report = new BarcodeRpt();
            


           
            EmptyBarcode = Convert.ToInt16(lblblank.Text);
            for (int x = 0; x < EmptyBarcode; x++)
            {
                DataRow drow = dataTable.NewRow();
                dataTable.Rows.Add(drow);
            }


            while (dataReader.Read())
            {

                DataRow drow = dataTable.NewRow();

                drow["ProductId"] = "*";
                drow["ProductId"] += dataReader["productid"].ToString();
                drow["ProductId"] += "*";

                 drow["ProductName"] = dataReader["productname"].ToString() + "(" + dataReader["productid"].ToString() + ")";
                drow["Cost"] = dataReader["mrp"].ToString() + "/-";
                drow["PartyName"] = dataReader["partyname"].ToString();
                drow["ShopName"] = "Shop Name";
                drow["size"] = dataReader["size"].ToString();

                dataTable.Rows.Add(drow);

            }
            
           Report.Database.Tables["BarcodeTable"].SetDataSource((DataTable)dataTable);
           Preview main1 = new Preview(Report);
            main1.Show();
            this.Close();
        }

        private void BarcodeReprint_Load(object sender, EventArgs e)
        {
            dataReader = salabs.DBMSQuerry("SELECT Max(productid) from product");
            while (dataReader.Read())
            {
                txtmax.Text = dataReader["Max(productid)"].ToString();
                txtmin.Text = minval.ToString();
                lblblank.Text = "0";

            }
        }
    }
}
