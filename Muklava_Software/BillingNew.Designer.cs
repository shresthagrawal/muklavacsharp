﻿namespace Muklava_Software
{
    partial class BillingNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.productid = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Print = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.GridView1 = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.Label();
            this.add = new System.Windows.Forms.Button();
            this.addview = new System.Windows.Forms.DataGridView();
            this.phonenumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CustomerName = new System.Windows.Forms.Label();
            this.start = new System.Windows.Forms.Button();
            this.tid = new System.Windows.Forms.Label();
            this.TotalLbl = new System.Windows.Forms.Label();
            this.CheckBtn = new System.Windows.Forms.Button();
            this.reedem = new System.Windows.Forms.Button();
            this.Points = new System.Windows.Forms.Label();
            this.pointslbl = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.discountvalue2 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.discountvalue = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAddCharges = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtpoints1 = new System.Windows.Forms.TextBox();
            this.txtpoints2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.addChargesLabelText = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addview)).BeginInit();
            this.SuspendLayout();
            // 
            // productid
            // 
            this.productid.Location = new System.Drawing.Point(149, 49);
            this.productid.Name = "productid";
            this.productid.Size = new System.Drawing.Size(178, 20);
            this.productid.TabIndex = 0;
            this.productid.TextChanged += new System.EventHandler(this.productid_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(90, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "ProductId";
            // 
            // Print
            // 
            this.Print.Location = new System.Drawing.Point(436, 409);
            this.Print.Name = "Print";
            this.Print.Size = new System.Drawing.Size(75, 23);
            this.Print.TabIndex = 2;
            this.Print.Text = "Print";
            this.Print.UseVisualStyleBackColor = true;
            this.Print.Click += new System.EventHandler(this.Print_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(346, 409);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // GridView1
            // 
            this.GridView1.AllowUserToAddRows = false;
            this.GridView1.AllowUserToDeleteRows = false;
            this.GridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridView1.Location = new System.Drawing.Point(35, 163);
            this.GridView1.Name = "GridView1";
            this.GridView1.ReadOnly = true;
            this.GridView1.Size = new System.Drawing.Size(434, 95);
            this.GridView1.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(404, 378);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Total:";
            // 
            // total
            // 
            this.total.AutoSize = true;
            this.total.Location = new System.Drawing.Point(343, 131);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(0, 13);
            this.total.TabIndex = 6;
            // 
            // add
            // 
            this.add.Location = new System.Drawing.Point(394, 134);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(75, 23);
            this.add.TabIndex = 7;
            this.add.Text = "add";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // addview
            // 
            this.addview.AllowUserToAddRows = false;
            this.addview.AllowUserToDeleteRows = false;
            this.addview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.addview.Location = new System.Drawing.Point(35, 75);
            this.addview.Name = "addview";
            this.addview.ReadOnly = true;
            this.addview.Size = new System.Drawing.Size(434, 53);
            this.addview.TabIndex = 8;
            // 
            // phonenumber
            // 
            this.phonenumber.Location = new System.Drawing.Point(93, 12);
            this.phonenumber.Name = "phonenumber";
            this.phonenumber.Size = new System.Drawing.Size(178, 20);
            this.phonenumber.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Phone Number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 419);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Transiction Id:";
            // 
            // CustomerName
            // 
            this.CustomerName.AutoSize = true;
            this.CustomerName.Location = new System.Drawing.Point(277, 15);
            this.CustomerName.Name = "CustomerName";
            this.CustomerName.Size = new System.Drawing.Size(80, 13);
            this.CustomerName.TabIndex = 12;
            this.CustomerName.Text = "Customer name";
            this.CustomerName.Click += new System.EventHandler(this.CustomerName_Click);
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(363, 12);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(75, 23);
            this.start.TabIndex = 13;
            this.start.Text = "Start";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // tid
            // 
            this.tid.AutoSize = true;
            this.tid.Location = new System.Drawing.Point(90, 333);
            this.tid.Name = "tid";
            this.tid.Size = new System.Drawing.Size(0, 13);
            this.tid.TabIndex = 14;
            // 
            // TotalLbl
            // 
            this.TotalLbl.AutoSize = true;
            this.TotalLbl.Location = new System.Drawing.Point(437, 378);
            this.TotalLbl.Name = "TotalLbl";
            this.TotalLbl.Size = new System.Drawing.Size(13, 13);
            this.TotalLbl.TabIndex = 15;
            this.TotalLbl.Text = "0";
            // 
            // CheckBtn
            // 
            this.CheckBtn.Location = new System.Drawing.Point(313, 134);
            this.CheckBtn.Name = "CheckBtn";
            this.CheckBtn.Size = new System.Drawing.Size(75, 23);
            this.CheckBtn.TabIndex = 16;
            this.CheckBtn.Text = "&Check";
            this.CheckBtn.UseVisualStyleBackColor = true;
            this.CheckBtn.Click += new System.EventHandler(this.CheckBtn_Click);
            // 
            // reedem
            // 
            this.reedem.Location = new System.Drawing.Point(275, 315);
            this.reedem.Name = "reedem";
            this.reedem.Size = new System.Drawing.Size(110, 23);
            this.reedem.TabIndex = 17;
            this.reedem.Text = "Reedem";
            this.reedem.UseVisualStyleBackColor = true;
            this.reedem.Click += new System.EventHandler(this.reedem_Click);
            // 
            // Points
            // 
            this.Points.AutoSize = true;
            this.Points.Location = new System.Drawing.Point(391, 315);
            this.Points.Name = "Points";
            this.Points.Size = new System.Drawing.Size(36, 13);
            this.Points.TabIndex = 18;
            this.Points.Text = "Points";
            // 
            // pointslbl
            // 
            this.pointslbl.AutoSize = true;
            this.pointslbl.Location = new System.Drawing.Point(424, 315);
            this.pointslbl.Name = "pointslbl";
            this.pointslbl.Size = new System.Drawing.Size(13, 13);
            this.pointslbl.TabIndex = 19;
            this.pointslbl.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(391, 349);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Discount";
            // 
            // discountvalue2
            // 
            this.discountvalue2.Location = new System.Drawing.Point(442, 346);
            this.discountvalue2.Name = "discountvalue2";
            this.discountvalue2.Size = new System.Drawing.Size(27, 20);
            this.discountvalue2.TabIndex = 22;
            this.discountvalue2.TextChanged += new System.EventHandler(this.discountvalue_TextChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(275, 344);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 23);
            this.button2.TabIndex = 23;
            this.button2.Text = "Discount";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // discountvalue
            // 
            this.discountvalue.AutoSize = true;
            this.discountvalue.Location = new System.Drawing.Point(462, 315);
            this.discountvalue.Name = "discountvalue";
            this.discountvalue.Size = new System.Drawing.Size(13, 13);
            this.discountvalue.TabIndex = 24;
            this.discountvalue.Text = "0";
            this.discountvalue.Click += new System.EventHandler(this.discountvalue_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(443, 315);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(12, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "/";
            // 
            // txtAddCharges
            // 
            this.txtAddCharges.Location = new System.Drawing.Point(394, 276);
            this.txtAddCharges.Name = "txtAddCharges";
            this.txtAddCharges.Size = new System.Drawing.Size(81, 20);
            this.txtAddCharges.TabIndex = 26;
            this.txtAddCharges.Text = "0";
            this.txtAddCharges.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(362, 46);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 28;
            this.button3.Text = "Create";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(35, 279);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 29;
            this.label8.Text = "Staff 1";
            // 
            // txtpoints1
            // 
            this.txtpoints1.Location = new System.Drawing.Point(76, 276);
            this.txtpoints1.Name = "txtpoints1";
            this.txtpoints1.Size = new System.Drawing.Size(81, 20);
            this.txtpoints1.TabIndex = 30;
            this.txtpoints1.Text = "0";
            this.txtpoints1.TextChanged += new System.EventHandler(this.txtpoints1_TextChanged);
            // 
            // txtpoints2
            // 
            this.txtpoints2.Location = new System.Drawing.Point(76, 312);
            this.txtpoints2.Name = "txtpoints2";
            this.txtpoints2.Size = new System.Drawing.Size(81, 20);
            this.txtpoints2.TabIndex = 32;
            this.txtpoints2.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(35, 315);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "Staff 2";
            // 
            // addChargesLabelText
            // 
            this.addChargesLabelText.Location = new System.Drawing.Point(228, 276);
            this.addChargesLabelText.Name = "addChargesLabelText";
            this.addChargesLabelText.Size = new System.Drawing.Size(157, 20);
            this.addChargesLabelText.TabIndex = 33;
            this.addChargesLabelText.Text = "Alteration or Additional Charges";
            // 
            // BillingNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 444);
            this.Controls.Add(this.addChargesLabelText);
            this.Controls.Add(this.txtpoints2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtpoints1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.txtAddCharges);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.discountvalue);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.discountvalue2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pointslbl);
            this.Controls.Add(this.Points);
            this.Controls.Add(this.reedem);
            this.Controls.Add(this.CheckBtn);
            this.Controls.Add(this.TotalLbl);
            this.Controls.Add(this.tid);
            this.Controls.Add(this.start);
            this.Controls.Add(this.CustomerName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.phonenumber);
            this.Controls.Add(this.addview);
            this.Controls.Add(this.add);
            this.Controls.Add(this.total);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.GridView1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Print);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.productid);
            this.Name = "BillingNew";
            this.Text = "billing";
            this.Load += new System.EventHandler(this.billing_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox productid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Print;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView GridView1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.DataGridView addview;
        private System.Windows.Forms.TextBox phonenumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label CustomerName;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Label tid;
        private System.Windows.Forms.Label TotalLbl;
        private System.Windows.Forms.Button CheckBtn;
        private System.Windows.Forms.Button reedem;
        private System.Windows.Forms.Label Points;
        private System.Windows.Forms.Label pointslbl;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox discountvalue2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label discountvalue;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAddCharges;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtpoints1;
        private System.Windows.Forms.TextBox txtpoints2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox addChargesLabelText;
    }
}