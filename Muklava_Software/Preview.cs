﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Muklava_Software
{
    public partial class Preview : Form
    {
        BarcodeRpt barcoderpt;
        public Preview(BarcodeRpt barcodeRptget)
        {
            barcoderpt = barcodeRptget;
            InitializeComponent();
        }

        private void Preview_Load(object sender, EventArgs e)
        {
            
            crystalReportViewer1.ReportSource = barcoderpt;
            crystalReportViewer1.Refresh();

        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
