﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Deployment.Application;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
 using Timer = System.Timers.Timer;


namespace Muklava_Software
{
    public partial class Main : Form
    {
        

        public Main()
        {
            InitializeComponent();
            //SyncroniseInBackGround();
            if(MuklavaFuntion.x!=1)
           MuklavaFuntion.CompleteSyncroniseInBackGround();
        }
       
        private void Main_Load(object sender, EventArgs e)
        {
           getProductNameAndPartyName();
           /* try
            {
                this.versionlbl.Text = Application.ProductVersion.ToString();
            }
            catch
            {

            }*/

            button2.Enabled = false;
            Timer t = new Timer(300000); // 1 sec = 1000, 60 sec = 60000

            t.AutoReset = true;

            t.Elapsed += new System.Timers.ElapsedEventHandler(t_Elapsed);

            t.Start();







        }
        static Boolean t = true;
        private static void t_Elapsed(object sender, System.Timers.ElapsedEventArgs e)

        {
            if (t != true)
                SyncroniseInBackGround();
            else
                t = false;

        }



        public static BackgroundWorker myWorker = new BackgroundWorker();
        public static List<string> productname = new List<string>();
        public static List<string> partyname = new List<string>();

        public void getProductNameAndPartyName()
        {
            myWorker.DoWork += new DoWorkEventHandler(myWorker_DoWork);
            myWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(completed);
            myWorker.WorkerSupportsCancellation = true;
            myWorker.RunWorkerAsync();
        }

        public static Button btn;
        public void completed(object sender, RunWorkerCompletedEventArgs e)
        {

            button2.Enabled = true;

        }
        public static String app;
        private static void myWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Salabs salabs = new Salabs();
            MySqlDataReader productName = salabs.DBMSQuerry("Select Distinct(productname) from product");
            while (productName.Read())
            {

                productname.Add(productName[0].ToString());
                
               
            }
            MySqlDataReader partyName = salabs.DBMSQuerry("Select Distinct(partyname) from product");
            while (partyName.Read())
            {
                partyname.Add(partyName[0].ToString());
                app = (partyName[0].ToString());
            }
        }

        public static BackgroundWorker myWorkerSync = new BackgroundWorker();
        public static void SyncroniseInBackGround()
        {

            myWorkerSync.DoWork += new DoWorkEventHandler(myWorker_DoWorkSync);
            myWorkerSync.RunWorkerAsync();

        }
        private static void myWorker_DoWorkSync(object sender, DoWorkEventArgs e)
        {
            
                //MessageBox.Show("hi");
                MuklavaFuntion.sycronise();
                
            
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (StockEntry.Webcam != null)
            {
                StockEntry.Webcam.Stop();
            }
            StockEntry stockEntry = new StockEntry();
            stockEntry.Show();

        }
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (StockEntry.Webcam != null)
            {
                StockEntry.Webcam.Stop();
            }
            System.Windows.Forms.Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BillingNew billing = new BillingNew();
            billing.Show();
           // invoicePrint billing = new invoicePrint("");
           // billing.Show();

        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void CustomerDetails_Click(object sender, EventArgs e)
        {
            CustomerData cst = new CustomerData();
            cst.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            dueamount da = new dueamount();
            da.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {


            MuklavaFuntion.CompleteSyncroniseInBackGround(sender);

        }

        private void reprint_Click(object sender, EventArgs e)
        {
            BarcodeReprint da = new BarcodeReprint();
            da.Show();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            StockEdit da = new StockEdit();
            da.Show();

        }

        private void createCustomer_Click(object sender, EventArgs e)
        {
            CreateCustomer cc = new CreateCustomer();
            cc.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            photosUpload ip = new photosUpload();
            ip.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            measurementRecord mr = new measurementRecord();
            mr.Show();
        }

        private void settings_Click(object sender, EventArgs e)
        {
           
            SyncroniseInBackGround();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            BillCancel billCancel = new BillCancel();
            billCancel.Show();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Salabs salabs = new Salabs();
            salabs.DBMSQuerry("Delete From dailyStockManagement.product; INSERT INTO dailyStockManagement.product SELECT * FROM muklavalocal.product;", sender,MuklavaFuntion.connStringGodaddy,1,0);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            GoodsReturn goodsReturn = new GoodsReturn();
            goodsReturn.Show();
        }
    }
}
