﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Muklava_Software
{
    public partial class CustomerData : Form
    {
        public CustomerData()
        {
            InitializeComponent();
        }
        DataTable dt = new DataTable();
        MySqlDataReader dataReader;
        Salabs salabs = new Salabs();

        private void CustomerData_Load(object sender, EventArgs e)
        {
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("PhoneNumber", typeof(string));
            dt.Columns.Add("Address", typeof(string));
            dt.Columns.Add("Points", typeof(int));
            dt.Columns.Add("Birthdate", typeof(string));
            dt.Columns.Add("Anniversary", typeof(string));

            CustomerDetails customerData = new CustomerDetails();
            DataTable dataTable = customerData.CustomerData;
            CustomerDataPrint Report = new CustomerDataPrint();

            dataReader = salabs.DBMSQuerry("SELECT Name,PhoneNumber,Address,Points,Birthdate,Anniversary FROM CustomerRecord");
            while (dataReader.Read())
            {
               
                dataTable.Rows.Add(dataReader["Name"], dataReader["PhoneNumber"], dataReader["Address"], dataReader["BirthDate"], (dataReader["Anniversary"].ToString()), (dataReader["Points"].ToString()));
                dt.Rows.Add(dataReader["Name"], dataReader["PhoneNumber"], dataReader["Address"],  dataReader["Points"], dataReader["BirthDate"], dataReader["Anniversary"]);

            }

            gdv.DataSource = dt;
            Report.Database.Tables["CustomerData"].SetDataSource((DataTable)dataTable);
           Cr = new CrsytalReportVeiwCustomerData(Report);
            

        }
        CrsytalReportVeiwCustomerData Cr;
        private void print_Click(object sender, EventArgs e)
        {
            Cr.Show();
            this.Close();
        }
    }
}
