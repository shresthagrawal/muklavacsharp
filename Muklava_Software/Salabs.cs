﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.IO;
using System.IO.Compression;
using System.Net.NetworkInformation;
using System.Diagnostics;
using System.ComponentModel;
using System.Net.Sockets;

namespace Muklava_Software
{
    
    class Salabs
    {
        MuklavaFuntion mf= new MuklavaFuntion();
        MySqlConnection Conn;
        //  public static String DbmsUsername;
        //public static String DbmsPassword;
        public static String FtpUsername;
        public static String FtpPassword;
        public static string Path = getdocumentspath();
        private static readonly HttpClient client = new HttpClient();



        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://clients3.google.com/generate_204"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static string GetLocalIPAddress()
        {
            string localIP;
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect("8.8.8.8", 65530);
                IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                localIP = endPoint.Address.ToString();
            }
            return localIP;
        }
        public MySqlDataReader DBMSQuerry(string qry, object sender = null, string connString = MuklavaFuntion.connStringmuklavaOffline,int showMessageBox=0,int sendToOnline=1) {
            if(connString.Equals(MuklavaFuntion.connStringmuklavaOffline)&& MuklavaFuntion.x==1)
                {
                connString = MuklavaFuntion.connStringmuklavaOffline1;
            }

            MySqlDataReader mySqlData = Query(qry, connString,sender);
            if (mySqlData != null)
            {
                if (sendToOnline == 1)
                { MuklavaFuntion.insertQuerry(qry); }
                if(showMessageBox==1)
                {
                    MessageBox.Show("Successfull");
                }
                return mySqlData;

            }
            else
            {
                MessageBox.Show("Database Error");
                return null;
            }
           
            
           

        }

        public MySqlDataReader Query(string qry,string connString ,object sender=null)
        {
            // mf.LocalDBMSQuerry(qry,sender);
            try
            {
                (sender as Button).Enabled = false;
            }
            catch
            {

            }
            //
            try
            {
                Conn = new MySqlConnection();
                Conn.ConnectionString = connString;
                int i= Conn.ConnectionTimeout ;
                Conn.Open();
                
                String query = qry;
                MySqlCommand cmd = new MySqlCommand(query, Conn);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                try
                {
                    (sender as Button).Enabled = true;
                }
                catch
                {

                }


               
                return dataReader;


            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                MySqlDataReader dataReader = null;
                return dataReader;

            }

        }
        public void FileUpload(String FtpUrl, String FileLoc)
        {
            Salabs.FtpUsername = "admin@muklava.com";
            Salabs.FtpPassword = "shresthharsh";
            using (WebClient client = new WebClient())
            {
                client.Credentials = new NetworkCredential(FtpUsername, FtpPassword);
                client.UploadFile(FtpUrl, FileLoc);

            }
            //  salabs.FileUpload("admin@muklava.com", "shresthharsh", "ftp://muklava.com/muklava/" + s.id + ".jpeg", @"D:\" + s.id + ".jpeg");

        }
        public void FileDownload(String FtpUrl, String FileLoc)
        {
            Salabs.FtpUsername = "admin@muklava.com";
            Salabs.FtpPassword = "shresthharsh";
            using (WebClient client = new WebClient())
            {
                client.Credentials = new NetworkCredential(FtpUsername, FtpPassword);
                client.DownloadFile(FtpUrl, FileLoc);

            }
            //  salabs.FileUpload("admin@muklava.com", "shresthharsh", "ftp://muklava.com/muklava/" + s.id + ".jpeg", @"D:\" + s.id + ".jpeg");

        }
        public static string readFile(String Path)
        {
            String result;

            System.IO.FileInfo file = new System.IO.FileInfo(Path);
            file.Directory.Create();
            try
            {
                result = System.IO.File.ReadAllText(file.FullName);
            }
            catch
            {

                result = "";
            }
            return result;
        }
        public static void writeFile(String path, String text, int StartFromEnd= 0)
        {
            System.IO.FileInfo file = new System.IO.FileInfo(path);
            file.Directory.Create(); // If the directory already exists, this method does nothing.
            if (StartFromEnd == 0)
            {
                
                System.IO.File.WriteAllText(file.FullName, text);
            }
            else
            {
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.Write(text);
                    
                }


            }
        }

        public static String getdocumentspath()
        {
            String path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            return path;
        }
        public static string getpath(string id)
        {
            return Salabs.Path + @id + @".jpeg";
        }
        public static async void postMethodAsync(string Url, string[,] parametre, int noOfParametre=8)
        {
            if (!CheckForInternetConnection())
            {
                return;
            }

            Dictionary<string, string> keyvaluepairs = new Dictionary<string, string>();
            for (int i = 0; i < noOfParametre; i++)
            {
                keyvaluepairs.Add(parametre[i, 0], parametre[i, 1]);
            }
            var values1 = keyvaluepairs;

            var content = new FormUrlEncodedContent(values1);

            var response = await client.PostAsync(Url, content);

            var responseString = await response.Content.ReadAsStringAsync();
            MessageBox.Show(responseString);
        }
        public static void createPushNotification(String query, String host, String db, String username, String password, String title, String message, String server_key)
        {
            string[,] a = new string[8, 2];

            a[0, 0] = "query";
            a[0, 1] = "select fcm_token from fcm_info" + query;
            a[1, 0] = "host";
            a[1, 1] = host;
            a[2, 0] = "db";
            a[2, 1] = db;
            a[3, 0] = "user";
            a[3, 1] = username;
            a[4, 0] = "password";
            a[4, 1] = password;
            a[5, 0] = "message";
            a[5, 1] = message;
            a[6, 0] = "title";
            a[6, 1] = title;
            a[7, 0] = "server_key";
            a[7, 1] = server_key;




            /*  a[0,0] ="query";
              a[0,1] = "select fcm_token from fcm_info";
              a[1,0] = "host";
              a[1,1] = "localhost";
              a[2,0] = "db";
              a[2,1] = "Muklava";
              a[3,0] = "user";
              a[3,1] = "muklava";
              a[4,0] = "password";
              a[4,1] = "shresthharsh";
              a[5,0] = "message";
              a[5,1] = "hi";
              a[6,0] = "title";
              a[6,1] = "byy";
              a[7,0] = "server_key";
              a[7,1] = "AAAAe7kDjKw:APA91bFoneYCu1uaAS-m9LvPC2tZmiBNn1E70B8InvisFlc2IAD551FygyFxwjyhNs-cFYgqMy2R1RJWzLEEgmo3YBDorBOX2QHn0HsT9pEHtmw08yfRLNhxv-_4h6o33QF6l20u5gMr";
                   */
            postMethodAsync("http://muklava.com/salabs/fcm/create_notification.php", a);
        }
        public string getFtpDirectoryList(String FtpUrl)
        {
            Salabs.FtpUsername = "admin@muklava.com";
            Salabs.FtpPassword = "shresthharsh";


            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(FtpUrl);
            request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

            // This example assumes the FTP site uses anonymous logon.
            request.Credentials = new NetworkCredential(FtpUsername, FtpPassword);
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            string val = reader.ReadToEnd();

            //Console.WriteLine("Directory List Complete, status {0}", response.StatusDescription);

            reader.Close();
            response.Close();
            return val;



            //  salabs.FileUpload("admin@muklava.com", "shresthharsh", "ftp://muklava.com/muklava/" + s.id + ".jpeg", @"D:\" + s.id + ".jpeg");

        }

        public string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card  
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            }
            return sMacAddress;
        }
        public static void extractFile(string zipPath, string extractPath)
        {
            //Add preference IO.Compresion and IO.Compresion.FileSystem
            //string startPath = @"c:\example\start";
            // string zipPath = @"c:\example\result.zip";
            // string extractPath = @"c:\example\extract";

            //ZipFile.CreateFromDirectory(startPath, zipPath);

            ZipFile.ExtractToDirectory(zipPath, extractPath);
        }
        public static void deleteFolder(string path)
        {
            try
            {
                FileInfo finfo = new FileInfo(path);
                if (finfo.Attributes == FileAttributes.Directory)
                {
                    //recursively delete directory
                    Directory.Delete(path, true);
                }
                else if (finfo.Attributes == FileAttributes.Normal)
                {
                    File.Delete(path);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }





    }
}
