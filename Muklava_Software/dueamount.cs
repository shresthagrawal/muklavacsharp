﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Muklava_Software
{
    public partial class dueamount : Form
    {
        string phonenumber;
        int tid=0;
        DataTable dt = new DataTable();
        MySqlDataReader dataReader;
        Salabs salabs = new Salabs();
        public dueamount()
        {
            InitializeComponent();
            tid = 0;
            tidtext.Text = tid.ToString();

        }
        public dueamount(string phonenumberget, int transictionidget)
        {
            InitializeComponent();
            phonenumber = phonenumberget;
            tid = transictionidget;
           // tidtext.Text = tid.ToString();


        }

        private void button2_Click(object sender, EventArgs e)
        {
            int SelectedIndex = checklistpaymentmenthod.SelectedIndex == -1 ? 0 : checklistpaymentmenthod.SelectedIndex;
            Boolean bool1 = true;
            if (SelectedIndex == 2)
            {
                dataReader = salabs.DBMSQuerry("SELECT credit FROM CustomerRecord WHERE phonenumber ='" + phonenumbertext.Text + "'");
                while (dataReader.Read())
                {


                    if (Convert.ToInt32(dataReader["credit"]) <= Convert.ToInt32(amounttext.Text))
                    {
                        bool1 = false;
                        MessageBox.Show("Enter Valid Details");
                    }
                    else
                    {
                        int creditamount = Convert.ToInt16(amounttext.Text);
                        salabs.DBMSQuerry("UPDATE CustomerRecord SET credit = credit-" + creditamount.ToString() + " WHERE phonenumber = '" + phonenumbertext.Text + "'");

                    }
                }
            }
           
            if (bool1 == true)
            {
                int length = dt.Rows.Count;
                bool exists=false;
                bool valueExists=false;
                int  credit = 0;
                for (int i = 0; i < length; i++)
                {
                   if(dt.Rows[i].Field<int>("Transiction Id") == Convert.ToInt32(tidtext.Text))
                    {
                        exists = true;
                        if(Convert.ToInt32(amounttext.Text) <= dt.Rows[i].Field<int>("Credit"))
                        {
                            valueExists = true;
                            credit = dt.Rows[i].Field<int>("Credit");
                        }
                        break;
                    }

                }

                    
                if (exists && valueExists)
                {
                    //MessageBox.Show("1");
                    salabs.DBMSQuerry("UPDATE CustomerTransiction SET credit = credit-" + amounttext.Text + " WHERE transictionid ='" + tidtext.Text + "'");
                    salabs.DBMSQuerry("INSERT INTO paymentrecipt SET  phonenumber = '" + phonenumbertext.Text + "', transictionid='" + tidtext.Text + "', amount='" + amounttext.Text + "', paymentmethod='" + checklistpaymentmenthod.Items[SelectedIndex].ToString() + "';");
                    MessageBox.Show("Successfull");
                    invoicePrint billing = new invoicePrint(tidtext.Text);
                    billing.Show();
                    this.Close();
                    
                }
                else 
                {
                           
                    if (exists && !valueExists)
                    {
                      //  MessageBox.Show("2");
                        salabs.DBMSQuerry("UPDATE CustomerTransiction SET credit = credit-" + amounttext.Text + " WHERE transictionid ='" + tidtext.Text + "'");
                        salabs.DBMSQuerry("INSERT INTO paymentrecipt SET  phonenumber = '" + phonenumbertext.Text + "', transictionid='" + tidtext.Text + "', amount='" + amounttext.Text + "', paymentmethod='" + checklistpaymentmenthod.Items[SelectedIndex].ToString() + "';");
                        int creditamount = Convert.ToInt16(amounttext.Text) -credit ;
                        salabs.DBMSQuerry("UPDATE CustomerRecord SET credit = credit+" + creditamount.ToString() + " WHERE phonenumber = '" + phonenumbertext.Text + "'");
                        MessageBox.Show("Successfull");
                        invoicePrint billing = new invoicePrint(tidtext.Text);
                        billing.Show();
                        this.Close();
                        

                    }
                    else
                    {
                        //MessageBox.Show("3");
                        int creditamount = Convert.ToInt16(amounttext.Text);
                        salabs.DBMSQuerry("UPDATE CustomerRecord SET credit = credit+" + creditamount.ToString() + " WHERE phonenumber = '" + phonenumbertext.Text + "'");
                        salabs.DBMSQuerry("INSERT INTO paymentrecipt SET  phonenumber = '" + phonenumbertext.Text + "', transictionid='" + tidtext.Text + "', amount='" + amounttext.Text + "', paymentmethod='" + checklistpaymentmenthod.Items[SelectedIndex].ToString() + "';");    
                        MessageBox.Show("Successfull");
                        invoicePrint billing = new invoicePrint(tidtext.Text);
                        billing.Show();
                        this.Close();
                        

                    }
                }
            }
                    
                
            
                
            
           



        }

        private void getdue(string phonenumber)
        {
            

            int total=0;
            dataReader = salabs.DBMSQuerry("SELECT TransictionId,CurrentDateTime,TotalPrice,credit FROM CustomerTransiction WHERE Number ='"+phonenumber+ "' Order by TransictionId DESC");
            
            while (dataReader.Read())
            {
                dt.Rows.Add(dataReader["TransictionId"], dataReader["CurrentDateTime"], dataReader["TotalPrice"], dataReader["credit"]);
                total += Convert.ToInt32(dataReader["credit"]);

            }
           // dt.Rows.Add("0", "Debit-Money", "0", "0");
            totaltext.Text = total.ToString();
            duegrid.DataSource = dt;



        }

        private void dueamount_Load(object sender, EventArgs e)
        {
            checklistpaymentmenthod.Items.Add("cash", CheckState.Unchecked);
            checklistpaymentmenthod.Items.Add("card", CheckState.Unchecked);
            checklistpaymentmenthod.Items.Add("debit", CheckState.Unchecked);

            dt.Columns.Add("Transiction Id", typeof(int));
            dt.Columns.Add("Date", typeof(string));
            dt.Columns.Add("TotalBillAmount", typeof(string));
            dt.Columns.Add("Credit", typeof(int));
            if (tid!=0)
            {
                phonenumbertext.Text = phonenumber;
                tidtext.Text = tid.ToString();
                getdue(phonenumber);

            }
           

        }

        private void getduebtn_Click(object sender, EventArgs e)
        {
            getdue(phonenumbertext.Text);
            dataReader = salabs.DBMSQuerry("SELECT credit FROM CustomerRecord WHERE phonenumber ='" + phonenumbertext.Text + "'");
            dataReader.Read();
            lblDbtAmnt.Text = "Debited Amount: " + dataReader["credit"].ToString();
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            invoicePrint billing = new invoicePrint(tidtext.Text);
            billing.Show();
            this.Close();
        }
    }
}
