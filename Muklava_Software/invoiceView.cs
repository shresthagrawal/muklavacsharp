﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Muklava_Software
{
    public partial class invoiceView : Form
    {
        
        public invoiceView(Invoice invoice)
        {
            InitializeComponent();
            
            crystalReportViewer1.ReportSource = invoice;
            crystalReportViewer1.Refresh();
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
