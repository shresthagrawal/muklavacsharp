﻿namespace Muklava_Software
{
    partial class StockEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sizetext = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.checklistbranch = new System.Windows.Forms.CheckedListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.partyname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.mrp = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bntCapture = new System.Windows.Forms.Button();
            this.txtid = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // sizetext
            // 
            this.sizetext.Location = new System.Drawing.Point(104, 226);
            this.sizetext.Name = "sizetext";
            this.sizetext.Size = new System.Drawing.Size(320, 20);
            this.sizetext.TabIndex = 42;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(33, 229);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 41;
            this.label7.Text = "Size";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 191);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 39;
            this.label2.Text = "Branch";
            // 
            // checklistbranch
            // 
            this.checklistbranch.FormattingEnabled = true;
            this.checklistbranch.Location = new System.Drawing.Point(104, 185);
            this.checklistbranch.Name = "checklistbranch";
            this.checklistbranch.Size = new System.Drawing.Size(320, 34);
            this.checklistbranch.TabIndex = 38;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(339, 274);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 23);
            this.button1.TabIndex = 37;
            this.button1.Text = "Edit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // partyname
            // 
            this.partyname.Location = new System.Drawing.Point(104, 109);
            this.partyname.Name = "partyname";
            this.partyname.Size = new System.Drawing.Size(320, 20);
            this.partyname.TabIndex = 35;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "PartyName";
            // 
            // mrp
            // 
            this.mrp.Location = new System.Drawing.Point(104, 148);
            this.mrp.Name = "mrp";
            this.mrp.Size = new System.Drawing.Size(320, 20);
            this.mrp.TabIndex = 36;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "M.R.P";
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(104, 66);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(320, 20);
            this.name.TabIndex = 32;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "Item_Name";
            // 
            // bntCapture
            // 
            this.bntCapture.Location = new System.Drawing.Point(248, 274);
            this.bntCapture.Name = "bntCapture";
            this.bntCapture.Size = new System.Drawing.Size(85, 23);
            this.bntCapture.TabIndex = 30;
            this.bntCapture.Text = "Check";
            this.bntCapture.UseVisualStyleBackColor = true;
            this.bntCapture.Click += new System.EventHandler(this.bntCapture_Click);
            // 
            // txtid
            // 
            this.txtid.Location = new System.Drawing.Point(104, 28);
            this.txtid.Name = "txtid";
            this.txtid.Size = new System.Drawing.Size(320, 20);
            this.txtid.TabIndex = 44;
            this.txtid.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(33, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 43;
            this.label5.Text = "Product_Id";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // StockEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 317);
            this.Controls.Add(this.txtid);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.sizetext);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checklistbranch);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.partyname);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.mrp);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.name);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bntCapture);
            this.Name = "StockEdit";
            this.Text = "StockEdit";
            this.Load += new System.EventHandler(this.StockEdit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox sizetext;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox checklistbranch;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox partyname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox mrp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bntCapture;
        private System.Windows.Forms.TextBox txtid;
        private System.Windows.Forms.Label label5;
    }
}