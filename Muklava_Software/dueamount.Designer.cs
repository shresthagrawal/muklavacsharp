﻿namespace Muklava_Software
{
    partial class dueamount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.phonenumbertext = new System.Windows.Forms.TextBox();
            this.getduebtn = new System.Windows.Forms.Button();
            this.duegrid = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.totaltext = new System.Windows.Forms.TextBox();
            this.tidtext = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.amounttext = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.paybtn = new System.Windows.Forms.Button();
            this.checklistpaymentmenthod = new System.Windows.Forms.CheckedListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lblDbtAmnt = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.duegrid)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "PhoneNumber:";
            // 
            // phonenumbertext
            // 
            this.phonenumbertext.Location = new System.Drawing.Point(97, 10);
            this.phonenumbertext.Name = "phonenumbertext";
            this.phonenumbertext.Size = new System.Drawing.Size(254, 20);
            this.phonenumbertext.TabIndex = 1;
            // 
            // getduebtn
            // 
            this.getduebtn.Location = new System.Drawing.Point(357, 8);
            this.getduebtn.Name = "getduebtn";
            this.getduebtn.Size = new System.Drawing.Size(75, 23);
            this.getduebtn.TabIndex = 2;
            this.getduebtn.Text = "GetDue";
            this.getduebtn.UseVisualStyleBackColor = true;
            this.getduebtn.Click += new System.EventHandler(this.getduebtn_Click);
            // 
            // duegrid
            // 
            this.duegrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.duegrid.Location = new System.Drawing.Point(16, 55);
            this.duegrid.Name = "duegrid";
            this.duegrid.Size = new System.Drawing.Size(633, 150);
            this.duegrid.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(490, 214);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Total:";
            // 
            // totaltext
            // 
            this.totaltext.Location = new System.Drawing.Point(530, 211);
            this.totaltext.Name = "totaltext";
            this.totaltext.Size = new System.Drawing.Size(119, 20);
            this.totaltext.TabIndex = 5;
            // 
            // tidtext
            // 
            this.tidtext.Location = new System.Drawing.Point(109, 337);
            this.tidtext.Name = "tidtext";
            this.tidtext.Size = new System.Drawing.Size(119, 20);
            this.tidtext.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 340);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Trasiction Id:";
            // 
            // amounttext
            // 
            this.amounttext.Location = new System.Drawing.Point(325, 337);
            this.amounttext.Name = "amounttext";
            this.amounttext.Size = new System.Drawing.Size(119, 20);
            this.amounttext.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(273, 340);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Amount:";
            // 
            // paybtn
            // 
            this.paybtn.Location = new System.Drawing.Point(573, 334);
            this.paybtn.Name = "paybtn";
            this.paybtn.Size = new System.Drawing.Size(75, 23);
            this.paybtn.TabIndex = 10;
            this.paybtn.Text = "Pay";
            this.paybtn.UseVisualStyleBackColor = true;
            this.paybtn.Click += new System.EventHandler(this.button2_Click);
            // 
            // checklistpaymentmenthod
            // 
            this.checklistpaymentmenthod.FormattingEnabled = true;
            this.checklistpaymentmenthod.Location = new System.Drawing.Point(16, 214);
            this.checklistpaymentmenthod.Name = "checklistpaymentmenthod";
            this.checklistpaymentmenthod.Size = new System.Drawing.Size(212, 94);
            this.checklistpaymentmenthod.TabIndex = 11;
            this.checklistpaymentmenthod.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(469, 334);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "View Invoice";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblDbtAmnt
            // 
            this.lblDbtAmnt.AutoSize = true;
            this.lblDbtAmnt.Location = new System.Drawing.Point(252, 214);
            this.lblDbtAmnt.Name = "lblDbtAmnt";
            this.lblDbtAmnt.Size = new System.Drawing.Size(85, 13);
            this.lblDbtAmnt.TabIndex = 13;
            this.lblDbtAmnt.Text = "Debited amount:";
            // 
            // dueamount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(661, 369);
            this.Controls.Add(this.lblDbtAmnt);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checklistpaymentmenthod);
            this.Controls.Add(this.paybtn);
            this.Controls.Add(this.amounttext);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tidtext);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.totaltext);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.duegrid);
            this.Controls.Add(this.getduebtn);
            this.Controls.Add(this.phonenumbertext);
            this.Controls.Add(this.label1);
            this.Name = "dueamount";
            this.Text = "dueamount";
            this.Load += new System.EventHandler(this.dueamount_Load);
            ((System.ComponentModel.ISupportInitialize)(this.duegrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox phonenumbertext;
        private System.Windows.Forms.Button getduebtn;
        private System.Windows.Forms.DataGridView duegrid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox totaltext;
        private System.Windows.Forms.TextBox tidtext;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox amounttext;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button paybtn;
        private System.Windows.Forms.CheckedListBox checklistpaymentmenthod;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblDbtAmnt;
    }
}